#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail

# yes we can pass a content type even though we do not send content :)
echo "Authorization: ${STACKIT_SERVICE_ACCOUNT}" > /tmp/curlheaders
echo "Content-Type: application/json" >> /tmp/curlheaders

ske_api() {
    url=$1
    shift
    curl "https://ske.api.eu01.stackit.cloud/v1/projects/a67baa55-a893-49bd-ba60-2f92d811369f/clusters${url}" -H @/tmp/curlheaders $@
}

# clusters to delete:
# 1. claimed clusters which do not have a claim timestamp set.
#    This means the integrationtest died sometime during claiming or the
#    claiming never finished. The cluster is not used in this case.
# 2. claimed clusters where the test has started > 4h ago
# 3. any cluster where deletionTimestamp is not null
# 4. unclaimed clusters which are older than 1h and which are not healthy

toolroot="$(dirname "$0")/"
shoots_to_delete=()
all_shoots="$(ske_api "")"
now=$(date +%s)
stale_threshold_unix=$(( now - 3600*4 ))
broken_threshold_unix=$(( now - 3600*1 ))
stale_threshold=$(date -Iseconds --date="@$stale_threshold_unix")
broken_threshold=$(date -Iseconds --date="@$broken_threshold_unix")
printf 'Deleting broken clusters older than %s\n' "$broken_threshold"
printf 'Deleting stale clusters older than %s\n' "$stale_threshold"

while IFS=$'\t' read -r shootname is_deleted is_claimed claim_timestamp_unix; do
  printf 'Looking at cluster %q:\n' "$shootname"
  if [ "$is_claimed" == 'true' ]; then
      if [ "$claim_timestamp_unix" == "-" ]; then
          printf '  the shoot cluster does not have a claimed at timestamp. Something is broken here\n'
      else
          printf '  claimed at %s\n' "$(date -Iseconds --date="@$claim_timestamp_unix")"
      fi
  fi
  if [ "$is_deleted" == 'true' ]; then
      printf '  deletion in progress\n'
  fi

  if [ "$is_claimed" == 'true' ]; then
      if [ "$claim_timestamp_unix" == "-" ]; then
          printf '  deleting because: claimed, but no claim timestamp set\n'
          shoots_to_delete+=("$shootname")
      elif (( claim_timestamp_unix < stale_threshold_unix )); then
          printf '  deleting because: claimed && claim older than %s\n' "$stale_threshold"
          shoots_to_delete+=("$shootname")
      fi
  elif [ "$is_deleted" == 'true' ]; then
      printf '  deleting because: already deleting\n'
      shoots_to_delete+=("$shootname")
  fi

done <<<"$(jq -r '.items[] | (.name + "\t" + (.status.aggregated == "STATE_DELETING" | tostring) + "\t" + (.nodepools[0].labels["yaook.cloud/integrationtest"] != null | tostring) + "\t" + (if .nodepools[0].labels["yaook.cloud/testtime"] then .nodepools[0].labels["yaook.cloud/testtime"] else "-" end) + "\t")' <<<"$all_shoots")"

for shootname in "${shoots_to_delete[@]}"; do
  if ! "$toolroot/nuke-cluster.sh" "$shootname"; then
    printf 'Failed to nuke cluster %s, moving on\n' "$shootname"
  fi
done

printf 'All elegible clusters deleted. I’m done here.\n'
