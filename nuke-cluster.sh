#!/bin/bash
##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
set -euo pipefail

ske_api() {
    url=$1
    shift
    curl "https://ske.api.eu01.stackit.cloud/v1/projects/a67baa55-a893-49bd-ba60-2f92d811369f/clusters${url}" -H @/tmp/curlheaders $@
}

shootname="$1"
toolroot="$(dirname "$0")/"
resourcetypes=(novacomputenodes amqpusers amqpservers mysqlusers mysqlservices cephblockpools keystoneusers keystoneendpoints)

printf 'Beginning deletion of shoot %s\n' "$shootname"
if ! ske_api "/$shootname"; then
  printf 'Failed to query shoot %s' "$shootname" >&2
  exit 1
fi

ske_api "/$shootname" -X DELETE || true

if ! ske_api "/$shootname/credentials" | jq -r '.kubeconfig' > shoot.yaml; then
  printf 'Failed to extract kubeconfig\n' >&2
  exit 1
fi

export KUBECONFIG="$(pwd)/shoot.yaml"
for resourcetype in "${resourcetypes[@]}"; do
  "$toolroot/strip-finalizers.sh" "$resourcetype" -n "default" &
done
wait

unset KUBECONFIG
ske_api "/$shootname" || true
